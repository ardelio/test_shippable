#!/bin/bash
TAG=`git log --pretty=oneline --decorate -1 | grep -oe "tag: v\.\d"`
echo $TAG
if [ "$TAG" ]; then
  echo 'yay'
fi
exit 0
